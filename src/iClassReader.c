#include "iClassReader.h"

#define MEGA_NUMERS true

// c_chFont1616 / ssd1306_draw_1616char
#define ARROW_LEFT 0x3B
#define ARROW_RIGHT 0x3C
#define ARROW_UP 0x3D
#define ARROW_DOWN 0x3E

typedef struct {
  uint8_t FacilityCode;
  uint16_t CardNumber;
} weigandRecord;

uint8_t lcdWidth = 128;
uint8_t lcdHeight = 64;

uint8_t iclass_key[8] = {0}; // NB: not the permuted version
uint8_t iclass_decryptionkey[16] = {0};

uint16_t oneMinute = 60 * 1000;
uint32_t timer;
uint16_t lastCardNumber = 0;

void loadKeys() {
  FIL fp;
  FRESULT fr;
  size_t bytesRead = 0;

  // iClass key used for reading
  fr = f_open(&fp, "iclass_key.bin", FA_READ);
  if (fr != FR_OK) {
    printf("Could not open file: %d\n", fr);
  }

  fr = f_read(&fp, iclass_key, sizeof(iclass_key), &bytesRead);
  if (fr != FR_OK) {
    printf("Could not read file %d\n", fr);
    f_close(&fp);
    return;
  }
  if (bytesRead != sizeof(iclass_key)) {
    printf("%d bytes read when %d expected\n", bytesRead, sizeof(iclass_key));
    f_close(&fp);
    return;
  }

  f_close(&fp);

  // iClass key used for decrypting
  fr = f_open(&fp, "iclass_decryptionkey.bin", FA_READ);
  if (fr != FR_OK) {
    printf("Could not open file: %d\n", fr);
  }

  fr = f_read(&fp, iclass_decryptionkey, sizeof(iclass_decryptionkey), &bytesRead);
  if (fr != FR_OK) {
    printf("Could not read file %d\n", fr);
    f_close(&fp);
    return;
  }
  if (bytesRead != sizeof(iclass_decryptionkey)) {
    printf("%d bytes read when %d expected\n", bytesRead, sizeof(iclass_decryptionkey));
    f_close(&fp);
    return;
  }

  f_close(&fp);
}

ReturnCode decrypt(uint8_t *enc_data, weigandRecord *record) {
  uint8_t key[32] = {0};
  memcpy(key, iclass_decryptionkey, sizeof(iclass_decryptionkey));

  mbedtls_des3_context ctx;
  mbedtls_des3_set2key_dec(&ctx, key);

  uint8_t dec_data[8] = {0};

  mbedtls_des3_crypt_ecb(&ctx, enc_data, dec_data);

  mbedtls_des3_free(&ctx);

  uint8_t *v4 = dec_data + 4;
  v4[0] = 0;

  uint32_t bot = v4[3] | (v4[2] << 8) | (v4[1] << 16) | (v4[0] << 24);

  record->CardNumber = (bot >> 1) & 0xFFFF;
  record->FacilityCode = (bot >> 17) & 0xFF;

  return ERR_NONE;
}

void resetScreen() {
  lastCardNumber = 0;
  ssd1306_clear_screen(0x00);

  uint8_t fontHeight = 16;
  for (size_t i = 0; i < 8; i++) {
    ssd1306_draw_1616char(16 * i, fontHeight * 0, ARROW_UP);
  }
  ssd1306_display_string(0, fontHeight * 1, " Place card/fob ", fontHeight, 1);
  ssd1306_display_string(0, fontHeight * 2, "   on reader.   ", fontHeight, 1);
  ssd1306_refresh_gram();
}

void iClassReader_setup() {
  // TODO: detect failure and write to screen.
  loadKeys();
  resetScreen();
}

void iClassReader_loop() {
  ReturnCode err;

  rfalIclassIdentifyRes idRes;
  rfalIclassSelectRes selRes;
  rfalIclassReadCheckRes rcRes;
  rfalIclassCheckRes chkRes;
  rfalIclassReadBlockRes readRes;

  uint8_t div_key[8] = {0};
  uint8_t mac[4] = {0};
  uint8_t ccnr[12] = {0};

  weigandRecord record;

  if (timerIsExpired(timer)) {
    // When the timer is up, clear the previous result both on display and in lastCardNumber
    if (lastCardNumber != 0) {
      resetScreen();
    }
  }

  err = rfalIclassPollerInitialize();
  if (err != ERR_NONE) {
    printf("rfalIclassPollerInitialize error %d\n", err);
    return;
  }

  err = rfalFieldOnAndStartGT();
  if (err != ERR_NONE) {
    printf("rfalFieldOnAndStartGT error %d\n", err);
    return;
  }

  err = rfalIclassPollerCheckPresence();
  if (err != ERR_RF_COLLISION) {
    return;
  }

  err = rfalIclassPollerIdentify(&idRes);
  if (err != ERR_NONE) {
    printf("rfalIclassPollerIdentify error %d\n", err);
    return;
  }

  err = rfalIclassPollerSelect(idRes.CSN, &selRes);
  if (err != ERR_NONE) {
    printf("rfalIclassPollerSelect error %d\n", err);
    return;
  }

  err = rfalIclassPollerReadCheck(&rcRes);
  if (err != ERR_NONE) {
    printf("rfalIclassPollerReadCheck error %d\n", err);
    return;
  }

  memcpy(ccnr, rcRes.CCNR, sizeof(rcRes.CCNR)); // last 4 bytes left 0
  diversifyKey(selRes.CSN, iclass_key, div_key);
  doMAC(ccnr, div_key, mac);

  err = rfalIclassPollerCheck(mac, &chkRes);
  if (err != ERR_NONE) {
    printf("rfalIclassPollerCheck error %d\n", err);
    return;
  }

  err = rfalIclassPollerReadBlockSeven(&readRes);
  if (err != ERR_NONE) {
    printf("rfalIclassPollerReadBlockSeven error %d\n", err);
    return;
  }

  err = decrypt(readRes.data, &record);
  if (err != ERR_NONE) {
    printf("decrypt error %d\n", err);
    return;
  }

  if (record.CardNumber != lastCardNumber) {
    lastCardNumber = record.CardNumber;

    char logLine[25] = {0};
    sprintf(logLine, "FC: %03d CN: %05d", record.FacilityCode, record.CardNumber);

    // For logging
    SDCard_WriteLog(logLine);
    printf("[SD] %s\n", logLine);

    char cn[6] = {0};
    sprintf(cn, "%05d", record.CardNumber);

    ssd1306_clear_screen(0x00);
    if (MEGA_NUMERS) {
      for (int i = 0; i < 5; i++) {
        ssd1306_draw_3216char(i * 16 + 24, lcdHeight / 4, cn[i]);
      }
    } else {
      //chSize = 12 => c_chFont1206
      //chSize = 16 => c_chFont1608
      uint8_t fontHeight = 16;
      uint8_t fontWidth = fontHeight / 2;
      size_t cnWidth = 5 * fontWidth;
      size_t cnHeight = 1 * fontHeight;
      uint8_t x = (lcdWidth - cnWidth) / 2;
      uint8_t y = (lcdHeight - cnHeight) / 2;
      ssd1306_display_string(x, y, cn, fontHeight, 1);
    }
    // 50 = 3/4 the screen height + (16 - 12)/2 to center the 12pt font in the 16pt line height
    ssd1306_display_string(0, 50, "Display clears in 60s", 12, 1);

    ssd1306_refresh_gram();
    timer = timerCalculateTimer(oneMinute);
  }
}
