# Uncomment lines below if you have problems with $PATH
#SHELL := /bin/bash
#PATH := /usr/local/bin:$(PATH)

all:
	platformio -c vim run

monitor:
	platformio device monitor -b 115200 -p /dev/tty.SLAB_USBtoUART

openocd:
	openocd -f interface/stlink-v2.cfg -f target/stm32f1x_stlink.cfg

format:
	clang-format -style=file -i src/*

upload:
	platformio -c vim run --target upload

clean:
	platformio -c vim run --target clean

program:
	platformio -c vim run --target program

uploadfs:
	platformio -c vim run --target uploadfs

update:
	platformio -c vim update
