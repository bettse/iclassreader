
#include "rfal_iclass.h"
#include "utils.h"

typedef struct {
  uint8_t CMD;
  uint8_t CSN[RFAL_ICLASS_UID_LEN];
} rfalIclassSelectReq;

typedef struct {
  uint8_t CMD;
  uint8_t null[4];
  uint8_t mac[4];
} rfalIclassCheckReq;

ReturnCode rfalIclassPollerInitialize(void) {
  ReturnCode ret;

  EXIT_ON_ERR(ret, rfalSetMode(RFAL_MODE_POLL_PICOPASS, RFAL_BR_26p48, RFAL_BR_26p48));
  rfalSetErrorHandling(RFAL_ERRORHANDLING_NFC);

  rfalSetGT(RFAL_GT_PICOPASS);
  rfalSetFDTListen(RFAL_FDT_LISTEN_PICOPASS_POLLER);
  rfalSetFDTPoll(RFAL_FDT_POLL_PICOPASS_POLLER);

  return ERR_NONE;
}

ReturnCode rfalIclassPollerCheckPresence(void) {
  ReturnCode ret;
  uint8_t txBuf[1] = {RFAL_ICLASS_CMD_ACTALL};
  uint8_t rxBuf[32] = {0};
  uint16_t recvLen = 0;
  uint32_t flags = RFAL_ICLASS_TXRX_FLAGS;
  uint32_t fwt = rfalConvMsTo1fc(20);

  ret = rfalTransceiveBlockingTxRx(txBuf, 1, rxBuf, 32, &recvLen, flags, fwt);
  return ret;
}

ReturnCode rfalIclassPollerIdentify(rfalIclassIdentifyRes *idRes) {
  ReturnCode ret;

  uint8_t txBuf[1] = {RFAL_ICLASS_CMD_IDENTIFY};
  uint16_t recvLen = 0;
  uint32_t flags = RFAL_ICLASS_TXRX_FLAGS;
  uint32_t fwt = rfalConvMsTo1fc(20);

  ret = rfalTransceiveBlockingTxRx(txBuf, sizeof(txBuf), (uint8_t *)idRes, sizeof(rfalIclassIdentifyRes), &recvLen, flags, fwt);
  // printf("identify rx: %d %s\n", recvLen, hex2Str(idRes->CSN, RFAL_ICLASS_UID_LEN));

  return ret;
}

ReturnCode rfalIclassPollerSelect(uint8_t *csn, rfalIclassSelectRes *selRes) {
  ReturnCode ret;

  rfalIclassSelectReq selReq;
  selReq.CMD = RFAL_ICLASS_CMD_SELECT;
  ST_MEMCPY(selReq.CSN, csn, RFAL_ICLASS_UID_LEN);
  uint16_t recvLen = 0;
  uint32_t flags = RFAL_ICLASS_TXRX_FLAGS;
  uint32_t fwt = rfalConvMsTo1fc(20);

  ret = rfalTransceiveBlockingTxRx((uint8_t *)&selReq, sizeof(rfalIclassSelectReq), (uint8_t *)selRes, sizeof(rfalIclassSelectRes), &recvLen, flags, fwt);
  // printf("select rx: %d %s\n", recvLen, hex2Str(selRes->CSN, RFAL_PICOPASS_UID_LEN));
  if (ret == ERR_TIMEOUT) {
    return ERR_NONE;
  }

  return ret;
}

ReturnCode rfalIclassPollerReadCheck(rfalIclassReadCheckRes *rcRes) {
  ReturnCode ret;
  uint8_t txBuf[2] = {RFAL_ICLASS_CMD_READCHECK, 0x02};
  uint16_t recvLen = 0;
  uint32_t flags = RFAL_ICLASS_TXRX_FLAGS;
  uint32_t fwt = rfalConvMsTo1fc(20);

  ret = rfalTransceiveBlockingTxRx(txBuf, sizeof(txBuf), (uint8_t *)rcRes, sizeof(rfalIclassReadCheckRes), &recvLen, flags, fwt);
  // printf("readcheck rx: %d %s\n", recvLen, hex2Str(rcRes->CCNR, 8));

  if (ret == ERR_CRC) {
    return ERR_NONE;
  }

  return ret;
}

ReturnCode rfalIclassPollerCheck(uint8_t *mac, rfalIclassCheckRes *chkRes) {
  ReturnCode ret;
  rfalIclassCheckReq chkReq;
  chkReq.CMD = RFAL_ICLASS_CMD_CHECK;
  ST_MEMCPY(chkReq.mac, mac, 4);
  ST_MEMSET(chkReq.null, 0, 4);
  uint16_t recvLen = 0;
  uint32_t flags = RFAL_ICLASS_TXRX_FLAGS;
  uint32_t fwt = rfalConvMsTo1fc(20);

  // printf("check tx: %s\n", hex2Str((uint8_t *)&chkReq, sizeof(rfalIclassCheckReq)));
  ret = rfalTransceiveBlockingTxRx((uint8_t *)&chkReq, sizeof(rfalIclassCheckReq), (uint8_t *)chkRes, sizeof(rfalIclassCheckRes), &recvLen, flags, fwt);
  // printf("check rx: %d %s\n", recvLen, hex2Str(chkRes->mac, 4));
  if (ret == ERR_CRC) {
    return ERR_NONE;
  }

  return ret;
}

ReturnCode rfalIclassPollerReadBlock( uint8_t blockNum, rfalIclassReadBlockRes *readRes) {
  ReturnCode ret;

  uint8_t txBuf[4] = {RFAL_ICLASS_CMD_READ, blockNum, 0xcc, 0x47};
  uint16_t recvLen = 0;
  uint32_t flags = RFAL_ICLASS_TXRX_FLAGS;
  uint32_t fwt = rfalConvMsTo1fc(20);

  ret = rfalTransceiveBlockingTxRx(txBuf, sizeof(txBuf), (uint8_t *)readRes, sizeof(rfalIclassReadBlockRes), &recvLen, flags, fwt);
  // printf("check rx: %d %s\n", recvLen, hex2Str(readRes->data, RFAL_ICLASS_MAX_BLOCK_LEN));

  return ret;

}

// Since I don't know how to generate the CRC, I only expose reading block 7
ReturnCode rfalIclassPollerReadBlockSeven( rfalIclassReadBlockRes *readRes) {
  return rfalIclassPollerReadBlock(7, readRes);
}
