
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _Browser_H
#define _Browser_H

/* Includes ------------------------------------------------------------------*/
#include "Fonts.h"
#include "GUI_Paint.h"
#include "Key.h"
#include "SD_Card_APP.h"
#include "SSD1306.h"
#include "demo.h"
#include "fatfs.h"
#include "rfal_nfca.h"
#include "rfal_rf.h"
#include "stdbool.h"
#include "string.h"
#include "tim.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
uint8_t Browser_save_fileame(uint8_t *path);
void Browser_display(void);
uint8_t Start_Drawing(void);
#endif
